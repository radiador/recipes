//
//  RecipeCollectionViewCell.swift
//  Recipes
//
//  Created by formador on 20/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class RecipeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var nameLb: UILabel!
    
    func configureCell(recipe: Recipe) {
        
        recipeImageView.image = recipe.image
        nameLb.text = recipe.name
    }
}
