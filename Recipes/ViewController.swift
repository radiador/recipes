//
//  ViewController.swift
//  Recipes
//
//  Created by formador on 20/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var recipesCollectionView: UICollectionView!
    
    var recipes = [Recipe]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Regitramos una o mas clases de celdas para poder usarlas
        recipesCollectionView.register(UINib(nibName: "RecipeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "recipeCellIdentiifer")
        
        //Alimentamos el modelo
        recipes.append(Recipe(name: "Fabada", image: UIImage(named: "receta1.jpeg") ?? UIImage()))
        recipes.append(Recipe(name: "Cocido", image: UIImage(named: "receta2.jpg") ?? UIImage()))
        recipes.append(Recipe(name: "Pasta", image: UIImage(named: "receta3.jpg") ?? UIImage()))
        recipes.append(Recipe(name: "Arroz", image: UIImage(named: "receta4.jpg") ?? UIImage()))
        recipes.append(Recipe(name: "Lentejas", image: UIImage(named: "receta5.jpeg") ?? UIImage()))

    }
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return recipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Recuperamos una celda de la tabla, si no hay ninguna en memoria se instanciara
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeCellIdentiifer", for: indexPath) as? RecipeCollectionViewCell
        
        //Configuramos la celda
        cell?.configureCell(recipe: recipes[indexPath.row])
        
        return cell ?? UICollectionViewCell()
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        //Se puede especificar el tamaño de las celdas de manera personalizada
        return  CGSize(width: 280, height: 280)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        //Los edge insets equivalen al pading en Android
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}

