//
//  Recipe.swift
//  Recipes
//
//  Created by formador on 21/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation
import UIKit

class Recipe {
    
    let name: String
    let image: UIImage
    
    init(name: String, image: UIImage) {
        
        self.name = name
        self.image = image
    }
}
